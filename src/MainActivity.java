public class MainActivity {

    public static void main(String[] args) {
        // OuterClass outer = new OuterClass();
        // outer.test();

        int[] a1 = { 10 };
        int[] a2 = { 2, 4, 6, 8 };
        int[] a3 = {};

        varArgs(a1);
        varArgs(a2);
        varArgs(a3);
        for (int i = 0; i < args.length; i++) {
            System.out.println("Main Args [" + i + "] Is : " + args[i]);
        }

    }


    private static void varArgs(int... v) {
        System.out.print("Number of Argoman is : " + v.length + " And Content is : ");

        for (int x: v) {
            System.out.print(x);
        }
        System.out.println();
    }
}
