public class OuterClass {

    int outer_var = 123;


    public void test() {
        for (int i = 0; i < 10; i++) {

            class InnerClass {

                public void display() {

                    System.out.println("X : " + outer_var);

                }
            }

            InnerClass inner = new InnerClass();
            inner.display();
        }
    }
}
